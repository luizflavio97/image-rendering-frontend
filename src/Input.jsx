import React from 'react'

export default props => {

    return (
        <div>
            <br />
            <label>{props.label}</label>
            <input type={props.type}
                onChange={props.method} />
            <br />
        </div>
    )
}
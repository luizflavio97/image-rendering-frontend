import React from 'react'
import './progressBar.css'

export default props => {
    return <div className = 'filler' style = {{width: `${props.percentage}%`}}></div>
}
import React, { Component } from 'react'
import axios from 'axios'

import Input from './Input'
import ProgressBar from './ProgressBar'

import './Form.css'

const URL = 'https://quantizate-image.herokuapp.com/api/image'

export default class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            photo: null,
            colors: '',
            grayscale: '',
            pixelizationAmount: '',
            uploadProgress: 0,
            showUploadProgress: false,
            finalImage: null
        }

        this.fileSended = this.fileSended.bind(this)
        this.colorsSeted = this.colorsSeted.bind(this)
        this.grayscaleSeted = this.grayscaleSeted.bind(this)
        this.pixelizationSeted = this.pixelizationSeted.bind(this)

        this.submitForm = this.submitForm.bind(this)

    }

    fileSended = e => {
        this.setState({
            photo: e.target.files[0]
        })
    }

    colorsSeted = e => {
        this.setState({
            colors: e.target.value
        })
    }

    grayscaleSeted = e => {
        this.setState({
            grayscale: e.target.value
        })
    }

    pixelizationSeted = e => {
        this.setState({
            pixelizationAmount: e.target.value
        })
    }

    submitForm = () => {
        let formData = new FormData()
        formData.append('photo', this.state.photo)
        formData.append('colors', this.state.colors)
        formData.append('grayscale', this.state.grayscale)
        formData.append('pixelizationAmount', this.state.pixelizationAmount)

        this.setState({
            showUploadProgress: true,
            uploadProgress: 0
        })

        axios({
            method: 'POST',
            url: URL,
            data: formData,
            config: {
                headers: { 'Content-Type': 'multipart/form-data' }
            },
            onUploadProgress: progressEvent => {
                this.setState({
                    uploadProgress: Math.round(progressEvent.loaded / progressEvent.total * 100)
                })
            }
        })
            .then(res => {
                this.setState({
                    finalImage: res.data.base64,
                    showUploadProgress: false
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div>
                <form className='form'>
                    <Input label='' type='file' method={this.fileSended} />
                    <Input label='Colors: ' type='input' method={this.colorsSeted} />
                    <Input label='Grayscale (true/false): ' type='input' method={this.grayscaleSeted} />
                    <Input label='Pixelization (0-1): ' type='input' method={this.pixelizationSeted} />
                    <div style={{ marginTop: 12 }}>
                        {this.state.showUploadProgress ? (
                            <ProgressBar percentage={this.state.uploadProgress} />
                        ) : (
                                <button type='button' onClick={this.submitForm}>Enviar</button>
                            )}
                    </div>
                </form>

                {this.state.finalImage !== null && (
                    <div style={{ marginTop: 16 }}>
                        <img
                            style={{ maxWidth: "90%" }}
                            src={`data:image/jpeg;base64,${this.state.finalImage}`} />
                    </div>
                )}
            </div>
        )
    }
}

